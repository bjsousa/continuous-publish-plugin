// Script executed by Jenkins build
// Get project version version and assign to ARTIFACT_VERSION parameter
import hudson.model.*
import java.util.jar.*
def build = Thread.currentThread().executable

Manifest manifest = new Manifest(
  new FileInputStream(
    new File("${build.workspace}/build/tmp/jar/MANIFEST.MF")));

def version = manifest.getMainAttributes().getValue(Attributes.Name.IMPLEMENTATION_VERSION)
println "Artifact version is $version"
build.addAction(
  new ParametersAction([
    new StringParameterValue("ARTIFACT_VERSION", version),
  ])
)
