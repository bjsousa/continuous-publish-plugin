package edu.wisc.doit.gradle

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Test

import static org.junit.Assert.assertTrue

/**
 * Unit tests for {@link ContinuousPublishPlugin}.
 *
 * @author Nicholas Blair
 */
class ContinuousPublishPluginTest {

  @Test
  public void pluginAddsTaskToProject() {
    Project project = ProjectBuilder.builder().build()
    project.pluginManager.apply 'java'
    project.pluginManager.apply 'edu.wisc.doit.gradle.continuous-publish-plugin'

    assertTrue(project.tasks.confirmProjectVersionIncremented instanceof ConfirmProjectVersionIncrementedTask)
  }

  @Test
  public void plugin_still_initializes_if_neither_java_or_war_plugin_present() {
    Project project = ProjectBuilder.builder().build()
    project.pluginManager.apply 'edu.wisc.doit.gradle.continuous-publish-plugin'

    assertTrue(project.tasks.confirmProjectVersionIncremented instanceof ConfirmProjectVersionIncrementedTask)
  }

}
