package edu.wisc.doit.gradle

import com.github.zafarkhaja.semver.Version
import org.gradle.api.GradleException

import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Encapsulates logic for comparing version strings.
 *
 * @author Nicholas Blair
 */
class CompareVersions {

  static final Pattern PATTERN = Pattern.compile(".*(\\d+\\.\\d+\\.\\d+).*")
  /**
   *
   * @param fromGitDescribe the version string from git (e.g. 'git describe')
   * @param inBuildGradle the version found in the project version property
   * @return none
   * @throws org.gradle.api.GradleException if version inBuildGradle is equal to or less than fromGitDescribe
   */
  static def compare(String fromGitDescribe, String inBuildGradle) {
    if(!fromGitDescribe?.trim()) {
      println "git describe is empty - there are no releases yet. You must have at least one tag on the repository to use this task."
      return
    }
    def lastVersion = parseVersionFromGitDescribe(fromGitDescribe)

    // read 'version' field from build.gradle
    def currentVersion = Version.valueOf(inBuildGradle);
    if ( currentVersion.greaterThan(lastVersion) ) {
      // success, the project version is greater than 'last.released.version'
      println "Version test successful: proposed version is " + currentVersion + " greater than last.released.version " + lastVersion
    } else {
      throw new GradleException("version field in build.gradle with current value " + currentVersion + " must be incremented past " + lastVersion)
    }
  }

  /**
   *
   * @param gitDescribe the version string from git
   * @return a {@link Version}
   * @throws IllegalArgumentException if a version could not be extracted
     */
  static Version parseVersionFromGitDescribe(String gitDescribe) {
    Matcher m = PATTERN.matcher(gitDescribe);
    if(m.matches()) {
      String substring = m.group(1);
      println "Extracted ${substring} from git describe output of ${gitDescribe}"
      return Version.valueOf(substring);
    }
    throw new IllegalArgumentException("cannot parse version from " + gitDescribe)
  }
}
