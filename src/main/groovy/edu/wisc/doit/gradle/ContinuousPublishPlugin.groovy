package edu.wisc.doit.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task

/**
 * Root Gradle {@link Plugin} class.
 *
 * Referenced in META-INF/gradle-plugins/edu.wisc.doit.gradle.continuous-publish-plugin.properties.
 *
 * @author Nicholas Blair
 */
class ContinuousPublishPlugin implements Plugin<Project> {
  @Override
  void apply(Project project) {
    project.getExtensions()
      .create("cpublish",
        ContinuousPublishPluginExtension.class)
    project.getTasks()
      .create("confirmProjectVersionIncremented",
        ConfirmProjectVersionIncrementedTask.class)

    // findByName can return null; getByName throws UnknownTaskException
    configureManifest(project.getTasks().findByName("jar"))
    configureManifest(project.getTasks().findByName("war"))
  }

  /**
   * Set 'Implementation-Title' and 'Implementation-Version' attributes
   * in the task's manifest
   *
   * See https://docs.oracle.com/javase/tutorial/deployment/jar/packageman.html
   *
   * @param task the task to configure (null safe)
   */
  void configureManifest(Task task) {
    task?.configure {
      manifest {
        attributes(
          'Implementation-Title': project.name,
          'Implementation-Version': project.version
        )
      }
    }
  }
}
