package edu.wisc.doit.gradle

import org.ajoberstar.grgit.Grgit
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

/**
 * Provides 'confirmProjectVersionIncremented' task.
 *
 * @see CompareVersions
 * @author Nicholas Blair
 */
class ConfirmProjectVersionIncrementedTask extends DefaultTask {

  @TaskAction
  def confirmProjectVersionIncremented() {
    if (!project.cpublish.skip) {
      File directory = getProject().getProjectDir()
      def grgit = Grgit.open(dir: directory)

      CompareVersions.compare(grgit.describe(), getProject().getVersion())
    }
  }

}
